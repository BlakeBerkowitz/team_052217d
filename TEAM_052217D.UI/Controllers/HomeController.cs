﻿using System.Web.Mvc;

namespace TEAM_052217D.UI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Notes()
        {
            ViewBag.Message = "notes about source contol";
            return View();
        }
    }
}
